workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  SPECIAL_ACTION:
    value: "no"
    description: Allow to run special protected jobs (destroy, ...). Set to "yes" to enable.

stages:
  - destroy
  - prepare
  - validate
  - build
  - deploy
  - install
  - configure

.tf_base:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  variables:
    TF_ROOT: terraform/${CI_ENVIRONMENT_SLUG}
    TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_PATH_SLUG}-${CI_ENVIRONMENT_SLUG}
  cache:
    key: "${CI_PROJECT_PATH_SLUG}-${CI_ENVIRONMENT_SLUG}"
    paths:
      - ${TF_ROOT}/.terraform
  before_script:
    - cd ${TF_ROOT}

.ansible_base:
  image: $CI_REGISTRY/cwxlab-fr/library/containers-images/ansible:latest
  variables:
    ANSIBLE_ROOT: ansible
  before_script:
    - mkdir ~/.ssh && cp "${SSH_CONFIG}" ~/.ssh/config
    - echo "${BASTION_KEY}" > ~/.ssh/bastion_id && chmod 600 ~/.ssh/bastion_id
    - cp "${HOST_KEY}" ~/.ssh/host_id && chmod 600 ~/.ssh/host_id
    - cd ${ANSIBLE_ROOT}

tf:destroy:production:
  extends:
    - .tf_base
  stage: destroy
  environment:
    name: production
  script:
    - gitlab-terraform destroy
  rules:
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual
    - when: never

tf:init:production:
  extends:
    - .tf_base
  stage: prepare
  environment:
    name: production
  script:
    - gitlab-terraform init
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual
    - changes:
      - ${TF_ROOT}/**/*
  needs: []

tf:validate:production:
  extends:
    - .tf_base
  stage: validate
  environment:
    name: production
  script:
    - gitlab-terraform validate
  needs: ["tf:init:production"]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: on_success
    - changes:
      - ${TF_ROOT}/**/*

tf:plan:production:
  extends:
    - .tf_base
  stage: build
  environment:
    name: production
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json
  needs: ["tf:validate:production"]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: on_success
    - changes:
      - ${TF_ROOT}/**/*

tf:apply:production:
  extends:
    - .tf_base
  stage: deploy
  environment:
    name: production
  script:
    - gitlab-terraform apply
  rules:
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: on_success
    - changes:
      - ${TF_ROOT}/**/*
  dependencies:
    - tf:plan:production
  needs: ["tf:plan:production"]

ansible:install:production:
  extends:
    - .ansible_base
  stage: install
  environment:
    name: production
  script:
    - ansible-playbook -i "inventories/${CI_ENVIRONMENT_SLUG}" runner.yml --tags install
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: delayed
      start_in: 5 minutes  # Delay for the cloud-init install
    - changes:
      - ${ANSIBLE_ROOT}/**/*

ansible:configure:production:
  extends:
    - .ansible_base
  stage: configure
  environment:
    name: production
  script:
    - ansible-playbook -i "inventories/${CI_ENVIRONMENT_SLUG}" runner.yml --extra-var "GITLAB_REGISTRATION_TOKEN=${GITLAB_REGISTRATION_TOKEN}" --tags configure
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: on_success
    - changes:
      - ${ANSIBLE_ROOT}/**/*

