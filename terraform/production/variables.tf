variable "ssh_public_keys" {
  type         = string
  description = "Public keys to add to the containers"
}

variable "pm_api_url" {
  type = string
}

variable "pm_password" {
  type = string
}

variable "pm_user" {
  type = string
}
