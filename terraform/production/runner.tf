module "runner" {

  source = "./modules/cwxlab-pve-vm"

  template = "template-ubuntu-focal-amd64-cloudinit"
  hostname = "runner01.grun.prod.priv.cwxlab.fr"
  node = "node301"
  pool = "GRUN"

  memory  = "2048"
  balloon = "1024"

  ipconfig0 = "ip=10.10.120.10/24,gw=10.10.120.240"

  bridges = ["vmbr120"]

  disks   = [
    {
      size: "32G",
      storage: "local"
    },
  ]

  user_name     = "grunner"
  user_ssh_keys = var.ssh_public_keys
}
